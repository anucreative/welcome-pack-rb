require 'csv'

def db_setup(db)
  # Create tables
  db.create_table :professions do
    primary_key :id
    String :name
    String :category_name
  end 

  db.create_table :jobs do
    primary_key :id
    foreign_key :profession_id, :professions
    String :contract_type
    String :name
    Float :office_latitude
    Float :office_longitude
  end 

  # Import CSVs
  professions = db[:professions]
  CSV.foreach('professions.csv', headers: true, header_converters: :symbol) do |row|
    professions.insert(row.to_hash)
  end

  jobs = db[:jobs]
  CSV.foreach('jobs.csv', headers: true, header_converters: :symbol) do |row|
    jobs.insert(row.to_hash)
  end
end