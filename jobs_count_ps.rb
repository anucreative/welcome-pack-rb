#!/usr/bin/env ruby

require 'pg'
require 'sequel'
require './db_setup'

# Create and connect to DB
conn = PG.connect(dbname: 'postgres')
conn.exec("DROP DATABASE IF EXISTS jobs")
conn.exec("CREATE DATABASE jobs")

DB = Sequel.connect('postgres://localhost/jobs')
db_setup(DB)

# Get contract_types
types = DB["SELECT DISTINCT contract_type FROM jobs ORDER BY contract_type"]
types_joined = types.map(:contract_type).join(",")
types_cast_joined = types.reduce("") { |a, type| "#{a} \"#{type[:contract_type]}\" text," }

# Make pivot (crosstab) function available
DB.run("CREATE EXTENSION tablefunc;")

# Get data from DB
pivot = DB["
  SELECT * FROM CROSSTAB(
    'SELECT professions.category_name AS category, jobs.contract_type AS type, COUNT (*) AS ct
    FROM jobs JOIN professions
    ON jobs.profession_id = professions.id
    GROUP BY category, type
    ORDER BY category, type'
    , $$SELECT unnest('{#{types_joined}}'::varchar[])$$
  ) AS result (\"category\" text, #{types_cast_joined.chomp(",")});
"]

# Display table
format = types.reduce("%-20s") { |a| "#{a} %-20s" }
puts format % types.map(:contract_type).unshift("")
pivot.each { |row| puts format % row.values.map { |v| v || 0 } }
