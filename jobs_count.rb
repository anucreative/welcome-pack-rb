#!/usr/bin/env ruby

require 'sequel'
require './db_setup'

# Create (in-memory) DB
DB = Sequel.sqlite
db_setup(DB)

# Get contract_types
types = DB["SELECT DISTINCT contract_type FROM jobs ORDER BY contract_type"]

# Select data for table
dataset = DB["
  SELECT category_name AS category, contract_type AS type, COUNT (*) AS ct
  FROM jobs JOIN professions
  ON jobs.profession_id = professions.id
  GROUP BY category, type
  ORDER BY category, type
"]

# Create new array from dataset
by_category = Hash.new

dataset.map do |row|
  key = row[:category]
  if by_category[key] == nil
    by_category[key] = []
  end
  by_category[key].push(row)
end

# Display table
format = types.reduce("%-20s") { |a| "#{a} %-20s" }
puts format % types.map(:contract_type).unshift("")
by_category.each do |k, records|
  # Fill in any missing contract_types
  data = types.map do |type|
    match = records.find do |record|
      record[:type] == type[:contract_type]
    end
    match ? match[:ct] : 0
  end
  # And print
  puts format % data.unshift(k)
end
