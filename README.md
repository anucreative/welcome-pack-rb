## Quick start

```
git clone git@bitbucket.org:anucreative/welcome-pack-rb.git
cd welcome-pack-rb
bundle install

rake print_table # to print using in-memory db
rake print_crosstab # to print using Postgres db with Crosstab
```
